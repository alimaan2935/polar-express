
var counter = 0;
var h1 = "<h1>There is no Santa</h1>";
var c1 = "<p> <blockquote> There is no Santa, </blockquote> my friend had insisted, but i knew he was wrong. </p> <p> Late that night I did hear sounds, though not of ringing bells. From outside came the sound of hissing steam and squeaking metal. I looked through my window and saw a train standing perfectly still in front of my house. </p> ";

var h2 = "<h1>The Train</h1>";
var c2 = "<p>It was wrapped in an apron of steam. Snowflakes fell lightly around it. A conductor stood at the open door of one of the cars. He took a large pocket watch from his vest, then looked up at my window. I put on my slippers and robe. I tiptoed downstairs and out the door,</p>";

var h3 = "<h1>Are you coming?</h1>";
var c3 =  "<p> <blockquote>All aboard,</blockquote> the conductor cried out. I ran up to him.</p>" +
          "<p>'Well,' he said, 'are you coming?'</p>" +
          "<p>'Where?' I asked.</p>" +
          "<p>'Why, to the North Pole of course,' was his answer. 'This is the Polar Express.' I took his outstretched hand and he pulled me aboard.</p>";

var h4 = "<h1>Towards Noth Pole</h1>";
var c4 =  "<p>The train, was filled with other children, all in their pajamas and nightgowns. We sang Christmas carols and ate candies with nougat centers as white as snow. We drank hot cocoa as thick and rich as melted chocolate bars. </p>" +
          "<p>Outside, the lights of towns and villages flickered in the distance as the Polar Express raced northward.</p>";

var h5 = "<h1>Cold, dark forest</h1>";
var c5 =  "<p>Soon there were no more lights to be seen. We traveled through cold, dark forests, where lean wolves roamed and white-tailed rabbits hid from our train as it thundered through the quiet wilderness.</p>";

var h6 = "<h1>Like a car on a roller coaster</h1>";
var c6 =  "<p>We climbed mountains so high it seemed as if wc would scrape the moon. But the Polar Express never slowed down, Faster and faster we ran along, rolling over peaks and through valleys like a car on a roller coaster.</p>";

var h7 = "<h1>There is the North Pole</h1>";
var c7 =  "<p>The mountains turned into hills, the hills to snow-covered plains. We crossed a barren desert of ice — the Great Polar Tee Cap. Lights appeared in the distance. They looked like the lights of a strange ocean liner sailing on a frozen sea.</p>" +
          "<p>'There,' said the conductor, 'is the North Pole.'</p>";


$(document).ready(function() {
    $("#next").click(function () {
      if (counter == 0) {
        $(".story").html(h1 + c1);
        counter++;
      } else if (counter == 1) {
        $(".story").html(h2 + c2);
        counter++;
      } else if (counter == 2) {
        $(".story").html(h3 + c3);
        counter++;
      } else if (counter == 3) {
        $(".story").html(h4 + c4);
        counter++;
      } else if (counter == 4) {
        $(".story").html(h5 + c5);
        counter++;
      } else if (counter == 5) {
        $(".story").html(h6 + c6);
        counter++;
      } else if (counter == 6) {
        $(".story").html(h7 + c7);
        counter++;
      }

    });





});
